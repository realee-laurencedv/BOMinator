#!/usr/bin/python3

"""         External libs       """
#import kicad_netlist_reader
import pcbnew
import wx

class BOMinator(wx.Frame):

    __BOMINATOR_NAME = "BOMinator"

    def __init__(self, parent, board):
        wx.Frame.__init__(self, parent, title=__BOMINATOR_NAME)
        self.panel = wx.Panel(self)
        label = wx.StaticText(self.panel, label = "I am a label")
        button = wx.Button(self.panel, label="buttonz!!!", id=1)

        nets = board.GetNetsByName()
        self.netnames = []
                for netname, net in nets.items():
                    if (str(netname) == ""):
                        continue
                    self.netnames.append(str(netname))

        netcb = wx.ComboBox(self.panel, choices=self.netnames)
        netcb.SetSelection(0)

        netsbox = wx.BoxSizer(wx.HORIZONTAL)
        netsbox.Add(wx.StaticText(self.panel, label="Nets:"))
        netsbox.Add(netcb, proportion=1)

        modules = board.GetModules()
        self.modulenames = []
        for mod in modules:
            self.modulenames.append("{}({})".format(mod.GetReference(), mod.GetValue()))
        modcb = wx.ComboBox(self.panel, choices=self.modulenames)
        modcb.SetSelection(0)

        modsbox = wx.BoxSizer(wx.HORIZONTAL)
        modsbox.Add(wx.StaticText(self.panel, label="Modules:"))
        modsbox.Add(modcb, proportion=1)
        
        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(label,   proportion=0)
        box.Add(button,  proportion=0)
        box.Add(netsbox, proportion=0)
        box.Add(modsbox, proportion=0)
        
        self.panel.SetSizer(box)
        self.Bind(wx.EVT_BUTTON, self.OnPress, id=1)
        self.Bind(wx.EVT_COMBOBOX, self.OnSelectNet, id=netcb.GetId())
        self.Bind(wx.EVT_COMBOBOX, self.OnSelectMod, id=modcb.GetId())

    def OnPress(self, event):
        print(str(event)+"pressed!")

    def OnSelectNet(self, event):
        item = event.GetSelection()
        print("Net {} was selected".format(self.netnames[item]))

    def OnSelectMod(self, event):
        item = event.GetSelection()
        print("Module {} was selected".format(self.modulenames[item]))

def InitGui(board):
        BOMi = BOMinator(None, board)
        BOMi.Show(True)
        return BOMi


BOMi = InitGui(pcbnew.GetBoard())